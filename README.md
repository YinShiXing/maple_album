# maple_album

#### 介绍
- Qt实现的简易的小相册

#### 环境
- Win10
- Qt5.14及以上版本
- Mingw编译器

#### 效果
[传送门](https://blog.csdn.net/t13506920069/article/details/124894155?spm=1001.2014.3001.5502)

#### 更新

> 2022-08-04

- 上传滚动截图功能
- 修改照片选中后的边框样式
- 现在右键选中旋转区域才能旋转图片

### 注意点

- opencv的dll位于项目目录下的`3rdparty/lib`，需要将动态库放置到exe目录下

- 滚动截图不能截取动态页面或者大面积同色背景页面(也就是页面变化要大，无论是色彩还是页面上的东西)



